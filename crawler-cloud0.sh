#!/bin/bash

./bin/nutch inject urls

for (( c=1; c<=20000000000; c++ ))
do
   echo "Generating batchId"
   batchId=`date +%s`-$RANDOM
   ./bin/nutch generate -topN 300 -batchId $batchId
   ./bin/nutch fetch $batchId
   ./bin/nutch parse $batchId
   ./bin/nutch updatedb
   python score.py > score-round$c.txt
   python webgraph.py round$c
   python urls-count.py >> crawldb.txt 
   
done

