#
# (C) Do Le Quoc, 2014
#


import pycassa

import getopt
import sys

import pycassa
from pycassa.pool import ConnectionPool
from pycassa.util import OrderedDict

import struct

def print_map(level, dict):
    for key in dict.keys():
        value = dict[key]
        if type(value) == type(OrderedDict()):
            print indent(level), key, ": {"
            print_map(level+1, value)
            print indent(level), "}"
        elif key == "sig" or key == "psig" or key == "_csh_":
      # these don't render well even though we do decode
      # unicode to utf8, so converting to hex
            print indent(level), key, ":", quote(to_hex_string(value)), ","
        else:
            print indent(level), key, ":", quote(value), ","

def to_hex_string(s):
    chars = []
    for i in range(0, len(s)):
        chars.append(hex(ord(s[i:i+1]))[2:])
    return "".join(chars)

def quote(s):
    if not(s.startswith("\"") and s.endswith("\"")):
        return "".join(["\"", unicode(s).encode("utf8"), "\""])
    else:
        return s

def indent(level):
    return ("." * level * 2)




def normalize_url(url):
    sub = url.split(":")
    domain = sub[0]
    subdomain = domain.split(".")

    normaldomain = ""
    for part in subdomain:
       if part == subdomain[0]:
	   normaldomain = part
       else:
	   normaldomain = part + "." + normaldomain
    return "http://" + normaldomain + sub[1].strip('http')


###################################################################################################################

pool = pycassa.ConnectionPool('webpage', server_list=['10.101.0.30', '10.101.0.31', '10.101.0.14'], prefill=False)

##################### Get score ####################################################################################

column = pycassa.ColumnFamily(pool, 'f')

for fk, fv in column.get_range():
    try:
        key = str(quote(fk))
        print  "key:", quote(fk), ",", "Score : ", struct.unpack('>f', fv["s"])[0]
    except:
        continue

