#!/bin/bash

./bin/nutch inject urls

for (( c=1; c<=100000; c++ ))
do
   echo "Generating batchId"
   batchId=`date +%s`-$RANDOM
   ./bin/nutch generate -topN 1000 -batchId $batchId
   ./bin/nutch fetch $batchId 
   ./bin/nutch parse $batchId
   ./bin/nutch updatedb
   ./bin/nutch solrindex http://localhost:8983/solr/ -all
done
